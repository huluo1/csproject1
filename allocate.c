/*
 ============================================================================
 Name        : allocate.c
 Author      : LUOHUI
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <String.h>
#include <assert.h>

#define MAX 10
#define P_NUM 5 //5个进程，可改变
#define P_TIME 50//优先级初始值

struct task_data{
int arrivetime;
char name[10];
int needtime;
int memory;
};

enum state{            /*枚举类型*/
	ready,//就绪
	execute,//执行
	block,//阻塞
	finish//完成
};//设置进程的状态

struct pcb{             /*进程控制块数据结构*/
	char name[10];//进程名
	int arrivaltime;//到达时间
	int priority;//优先级
	int cputime;//cpu执行的时间
	int needtime;//进程执行所需时间
	int count;//  记录执行的次数
	int round;//轮转次数
	enum state process;
	struct pcb * next;//链表指针
};
typedef struct pcb pcb;

pcb * creat_process(struct task_data data){
	struct pcb *q;
	q=(struct pcb *)malloc(sizeof(struct pcb));//分配空间
	strcpy(q->name,data.name);//输入进程名
	q->arrivaltime = data.arrivetime;
	q->needtime = data.needtime;//输入进程所需时间
	q->cputime=0;//令cpu执行时间初始值为0
	q->priority=P_TIME-q->needtime;//令其优先级为刚开始的固定值50-已经执行的时间
	q->process=ready;//进程状态为就绪
	q->next = NULL;//当前指针位置的下一节点为空
	return q;
}

int get_proc_remaining(pcb *p,int n){
	int proc_remain = 0;
	pcb *t;
	t = p->next;
	while (t){
		if ( p != t)
			break;
		if (t->process == ready && t->arrivaltime != n){
			proc_remain ++;
		}
		t= t->next;
	}
	return proc_remain;
}

pcb * scherduling_RR(pcb *q,pcb *t){
	pcb *p;
	if (q == NULL){
		p = t;
		p->next = p;
		return p;
	}
	p = q;
	t->next = p->next;
	p->next = t;
	return p;
}

struct pcb * cpurun_RR(pcb *q,int quantum,int n ){
	pcb *p,*t;
	p = q;
	if(p->process == ready){
		p->process = execute;
		printf("%d,RUNNING,process_name=%s,remaining_time=%d\n",n,p->name,p->needtime - p->cputime);
		return p;
	}

	if (p->process == execute){
		p->cputime += quantum;
		if(p->cputime >= p->needtime ){
			p->cputime = p->needtime;
			p->process = finish;
			int proc_remain = get_proc_remaining(p,n);
			printf("%d,process_name=%s,proc_remaining=%d \n",n,p->name,proc_remain);
			printf("%d,FINISHED-\nPROCESS,process_name=%s\n",n,p->name);
		}
	}

	//查找下一个进程
	p = p->next;
	while(p){
		if(p->process == ready){
		   break;
		}
		if(p = q)
			break;
		p=p->next;
	}

	if (p == q){
		if (p->process == finish ) //没有要调度的程序
			return NULL;
		else
			return p;
	}
	else{
		q->process = ready;
		p->process = execute;
		printf("%d,RUNNING,process_name=%s,remaining_time=%d\n",n,p->name,p->needtime - p->cputime);
	}
	return p;
}

struct pcb * scherduling_sjf(pcb *q,pcb *t){
	pcb *p,*m,*n;
	int waiting = 0;
	int waiting_m = 0;
	int waiting_t = 0;

	if (q == NULL){
		p = t;
		return p;
	}

	m = q;
	p = q;
	while(m != NULL){
		if( m->process != ready){
			if (m->arrivaltime > waiting){
				waiting = q->arrivaltime;
			}
			waiting += m->needtime;
			n = m;
			m = m->next;
			continue;
		}


		if( waiting < m->arrivaltime){
			waiting_m = m->arrivaltime;
		}
		else
			waiting_m = waiting;

		if( waiting < t->arrivaltime){
			waiting_t = t->arrivaltime;
		}
		else
			waiting_t = waiting;

		//不同时到达，先到先运行
		if (waiting_m > waiting_t){
			break;
		}

		if (waiting_m < waiting_t){
			waiting = waiting_m + m->needtime;
			n = m;
			m = m->next;
			continue;
		}
		//同时到达
		if (m->needtime > t->needtime ){
			break;
		}

		if (m->needtime < t->needtime){
			waiting = waiting_m + m->needtime;
			n = m;
			m = m->next;
			continue;
		}

		//相同的时候 按name
		if (m->needtime == t->needtime){
			if (m->name <= t->name){
				break;
			}
			else{
				waiting = waiting_m + m->needtime;
				n = m;
				m = m->next;
				continue;
			}
		}
	}
	if (m == NULL){
		n->next = t;
		return p;
	}

	if ( m == q){
		t->next = p;
		return t;
	}

	n->next = t;
	t->next = m;
	return p;
}



int cpurun (pcb *q,int quantum,int n){
	pcb *t = q;
	while(t){
		if (t->process == finish){
			t = t->next;
			continue;
		}
		if (t->process == execute ){
			t->cputime += quantum;
			if(t->cputime >= t->needtime ){
				t->cputime = t->needtime;
				t->process = finish;
				int proc_remain = get_proc_remaining(t,n);
				printf("%d,process_name=%s,proc_remaining=%d \n",n,t->name,proc_remain);
				printf("%d,FINISHED-\nPROCESS,process_name=%s\n",n,t->name);
				t = t->next;
				continue;
			}
			return 1;
		}
		if(t->process == ready){

			t->process = execute;
			printf("%d,RUNNING,process_name=%s,remaining_time=%d \n",n,t->name,t->needtime);
			return 1;
		}
		t = t->next;
	}
	return 0;
}

void print_pcb (pcb *p){
	pcb *t =p;
	while(t){
		if (t == p)
			break;
		printf("%d %s %d %d\n",t->arrivaltime,t->name,t->cputime,t->needtime);
		t = t->next;
	}
}

void free_pcb(pcb *p){
	pcb *t =p;
	pcb *m;
	while(t){
		m = t;
		t = t->next;
		free(m);
	}
}



int main(int argc, char* argv[]) {
	setbuf(stdout,NULL);
	pcb * queue = NULL;
	pcb * t;
	pcb * last;
	char *s ="SJF";
	int quantum = 1;
	int runtime =0;

    int m;

    char opt;
    struct task_data task[128];
    char *string = "f:s:m:q:d::| :";
    char *filename = "/Users/luohui/eclipse-workspace/allocate/src/cases/task2/simple.txt";
    //char *filename = "/Users/luohui/eclipse-workspace/allocate/src/cases/task1/more-processes.txt";

    while ((opt = getopt(argc, argv,string))!= -1)
    {
        printf("opt = %c\t\t", opt);
        printf("optarg = %s\t\t",optarg);
        printf("optind = %d\t\t",optind);
        printf("argv[optind] = %s\n",argv[optind]);

        switch (opt){
        	case 'f':
        	  //filename = optarg;
              break;
        	case 's':
			  s = optarg;
              break;
        	case 'q':
        	  quantum = 3;
        	  break;
        }
        printf("%d, %s\n",quantum,s);



    }

    printf("%s",filename);
    FILE * f=fopen(filename,"r");
    assert(f != NULL);

    int i=0;
    while(fscanf(f,"%d %s %d %d",&task[i].arrivetime,&task[i].name,&task[i].needtime,&task[i].memory)!=EOF){
    	printf("%d %s %d %d\n",task[i].arrivetime,task[i].name,task[i].needtime,task[i].memory);//输出到显示器屏幕
    	i++;    //关闭文件
    }
    fclose(f);
    //fclose(w);
    int count = i;//进程数
    queue = creat_process(task[0]);
    queue->next = queue;
    while(runtime < 200){
    	last = queue;
    	queue = cpurun_RR(queue,quantum,runtime);
    	if (queue == NULL)
    		break;

    	//create processes on time
    	for(i=1;i<count;i++){
    		if(task[i].arrivetime >= runtime && task[i].arrivetime < runtime+quantum ){
    			//printf("runtime:%s %d \n", task[i].name,runtime);
    			t = creat_process(task[i]);
    			//queue =	scherduling_sjf( queue,t);
    			scherduling_RR(last,t);
    			//print_pcb (queue);
    		}
    	}
    	//if ( cpurun(queue,quantum,runtime) == 0 ){ //运行进程
    	//     break;
    	//}

    	runtime += quantum;
    }
    printf("Makespan %d",runtime);
    free_pcb (queue);
	return EXIT_SUCCESS;
}







